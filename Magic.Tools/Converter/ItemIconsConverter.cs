﻿using Magic.Tools.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Magic.Tools.Converter
{
    /// <summary>
    /// Boolean to visible converter
    /// </summary>
    public class ImageLoadConverter : System.Windows.Markup.MarkupExtension, IValueConverter
    {
        public ImageLoadConverter()
        {

        }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (value is ItemIcon itemIcon)
            {
                if (itemIcon.Image == null)
                {
                    try
                    {
                        var image = new BitmapImage();
                        image.BeginInit();
                        image.CacheOption = BitmapCacheOption.OnLoad;
                        image.UriSource = new Uri(itemIcon.FullFileName);
                        image.EndInit();
                        itemIcon.Image = image;
                    }
                    catch (Exception)
                    {

                    }
                }
                return itemIcon.Image;
            }
            return null;
        }













        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return null;
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }
}
