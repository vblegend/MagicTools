﻿using Magic.Tools.Models;
using Magic.Tools.Views;
using Magic.Uitity;
using System;
using System.Windows;
using System.Windows.Input;
using System.Windows.Interop;
using Magic.Core;
using System.Threading.Tasks;
using System.Threading;

namespace Magic.Tools
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindowModel Model { get; private set; }

        public MainWindow()
        {
            LoadingDialog loadingDialog = new LoadingDialog();
            loadingDialog.ShowDialog();

            this.DataContext = Model = new MainWindowModel();
            InitializeComponent();
            Model.OnClose += Model_OnClose;
            //FullScreenManager.RepairWpfWindowFullScreenBehavior(this);



        }

        private void Model_OnClose(object sender, WindowDestroyArgs e)
        {
            this.DialogResult = e.DialogResult;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        
        }

        private void ScrollViewer_RequestBringIntoView(object sender, RequestBringIntoViewEventArgs e)
        {
            e.Handled = true;
        }
    }
}