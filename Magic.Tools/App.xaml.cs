﻿using Magic.Tools.Views;
using Magic.Uitity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Magic.Tools
{
    /// <summary>
    /// App.xaml 的交互逻辑
    /// </summary>
    public partial class App : Application
    {
        public App()
        {

        }

        protected override void OnStartup(StartupEventArgs e)
        {


            //base.OnStartup(e);
            MainWindow mainwindow = new MainWindow();
            mainwindow.ShowDialog();

        }
    }
}
