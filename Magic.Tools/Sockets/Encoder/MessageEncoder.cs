﻿using DotNetty.Buffers;
using DotNetty.Codecs;
using DotNetty.Transport.Channels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raspberry.Printer.Sockets.Encoder
{
    //通用处理
    public class MessageEncoder : MessageToByteEncoder<Byte[]>
    {
        protected override void Encode(IChannelHandlerContext context, Byte[] message, IByteBuffer output)
        {
            //序列化类
            output.WriteBytes(message);
        }
    }
}
