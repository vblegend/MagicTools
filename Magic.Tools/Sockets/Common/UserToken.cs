﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Raspberry.Printer.Sockets.Common
{
    public interface UserToken//: SocketContext
    {

        /// <summary>  
        /// 客户端IP地址  
        /// </summary>  
        String IPAddress { get; }

        /// <summary>  
        /// 连接时间  
        /// </summary>  
        DateTime ConnectTime { get; }

        /// <summary>
        /// 用户数据
        /// </summary>
        Object UserData { get; set; }

        /// <summary>
        /// 发送数据到客户端
        /// </summary>
        /// <param name="data"></param>
        void Send(Byte [] data);

        Boolean Connected { get; }

    }
}
