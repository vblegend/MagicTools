﻿using Magic.Tools.Models;
using Magic.Uitity;
using System.Windows;
using Magic.Core;

namespace Magic.Tools.Views
{
    /// <summary>
    /// LoadingDialog.xaml 的交互逻辑
    /// </summary>
    public partial class LoadingDialog : Window
    {
        public LoadingDialogModel Model { get; private set; }

        public LoadingDialog()
        {

            InitializeComponent();
            Model = new LoadingDialogModel();
            this.DataContext = Model;
            Model.OnClose += Model_OnClose;
        }

        private void Model_OnClose(object sender, WindowDestroyArgs e)
        {
            this.DialogResult = e.DialogResult;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Model.InitializeModel();
        }
    }
}
