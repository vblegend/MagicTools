﻿using Magic.Tools.Models;
using Magic.Uitity;
using System.Windows;
using Magic.Core;

namespace Magic.Tools.Views
{
    /// <summary>
    /// SelectedItemIconsDialog.xaml 的交互逻辑
    /// </summary>
    public partial class ItemIconsDialog : Window
    {
        public ItemIconsDialogModel Model { get; private set; }



        public ItemIconsDialog()
        {
            Model = new ItemIconsDialogModel();
            InitializeComponent();
            this.DataContext = Model;
            Model.OnClose += Model_OnClose;
            FullScreenManager.RepairWpfWindowFullScreenBehavior(this);
        }

        private void Model_OnClose(object sender, WindowDestroyArgs e)
        {
            this.DialogResult = e.DialogResult;
        }
    }
}
