﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Magic.Tools.Document
{
    public class LocalizationDocument
    {
        public LocalizationDocument(String file)
        {
            localization_file = file;
            HeadersMap = new Dictionary<string, int>();
            Headers = new List<string>();
            Children = new List<LocalRecord>();
        }






        public LocalRecord CreateRecord(String key, String source)
        {
            var record = new LocalRecord(this, $"{key},{source},,,,");
            Children.Add(record);
            return record;
        }





        public void Load()
        {
            if (!File.Exists(this.localization_file))
            {
                File.WriteAllText(this.localization_file, this.DefaultLocalization);
            }
            var localtext = File.ReadAllText(this.localization_file);
            var lines = localtext.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);
            if (lines.Length == 0)
            {
                lines = new string[] { DefaultLocalization };
            }
            try
            {
                this.ParsingHeader(lines[0].Replace("?", ""));
                for (int i = 1; i < lines.Length; i++)
                {
                    var record = new LocalRecord(this, lines[i]);
                    Children.Add(record);
                }
            }
            catch (Exception)
            {
                this.CreateStandardTable();
                //MessageBox.Show("Localization.txt 数据异常已重建结构。 本地化数据已丢失，请谨慎保存。", "Localization.txt 警告", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        private void ParsingHeader(String header)
        {
            Headers.Clear();
            HeadersMap.Clear();
            var handers = header.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < handers.Length; i++)
            {
                HeadersMap.Add(handers[i], i);
                Headers.Add(handers[i]);
            }
            if (!Headers.Contains("schinese"))
            {
                HeadersMap.Add("schinese", HeadersMap.Keys.Count);
                Headers.Add("schinese");
            }
        }

        public void CreateStandardTable()
        {
            Headers.Clear();
            HeadersMap.Clear();
            var handers = DefaultLocalization.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < handers.Length; i++)
            {
                HeadersMap.Add(handers[i], i);
                Headers.Add(handers[i]);
            }
        }




        public Int32 HeaderCount
        {
            get
            {
                return Headers.Count;
            }
        }

        public void Save()
        {
            StringBuilder document = new StringBuilder();
            document.AppendLine(String.Join(",", HeadersMap.Keys));

            foreach (var item in Children)
            {
                document.AppendLine(item.ToString());
            }
            document = document.Replace("\r\n", "\r");

            File.WriteAllText(this.localization_file, document.ToString());
        }


        public LocalRecord Query(String key)
        {
            return (from s in Children where s.Key == key select s).SingleOrDefault();
        }




        public List<LocalRecord> Children { get; private set; }
        public List<String> Headers { get; private set; }
        public Dictionary<String, Int32> HeadersMap { get; private set; }
        public readonly String DefaultLocalization = "Key,Source,Context,Changes,English,schinese";
        public string localization_file { get; private set; }
    }
}