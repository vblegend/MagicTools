﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Magic.Tools.Common;
using Magic.Tools.Common.Progression;

namespace Magic.Tools.Document
{
    [XmlRoot("items")]
    public class ItemsDocument
    {
        [XmlElement(ElementName = "item")]
        public List<item> Children { get; set; }
    }


    [XmlRoot("blocks")]
    public class BlocksDocument
    {
        [XmlAttribute(AttributeName = "defaultDescriptionKey")]
        public String defaultDescriptionKey { get; set; }

        [XmlElement(ElementName = "block")]
        public List<block> Children { get; set; }
    }


    [XmlRoot("entity_classes")]
    public class EntityClassesDocument
    {

        [XmlElement(ElementName = "entity_class")]
        public List<entity_class> Children { get; set; }

    }



    [XmlRoot("item_modifiers")]
    public class ItemModifiersDocument
    {

        [XmlElement(ElementName = "item_modifier")]
        public List<item_modifier> Children { get; set; }

    }





    [XmlRoot("vehicles")]
    public class VehiclesDocument
    {

        [XmlElement(ElementName = "vehicle")]
        public List<vehicle> Children { get; set; }

    }





    [XmlRoot("recipes")]
    public class RecipesDocument
    {

        [XmlElement(ElementName = "recipe")]
        public List<recipe> Children { get; set; }

    }

    [XmlRoot("Sounds")]
    public class SoundsDocument
    {

        [XmlElement(ElementName = "SoundDataNode")]
        public List<SoundDataNode> Children { get; set; }


    }



    [XmlRoot("buffs")]
    public class BuffsDocument
    {
        [XmlElement(ElementName = "buff")]
        public List<buff> Children { get; set; }
    }



    [XmlRoot("progression")]
    public class ProgressionDocument
    {
        [XmlElement(ElementName = "level")]
        public level level { get; set; }


        [XmlElement(ElementName = "attributes")]
        public attributes Attributes { get; set; }


        [XmlElement(ElementName = "skills")]
        public skills Skills { get; set; }


        [XmlElement(ElementName = "perks")]
        public perks Perks { get; set; }

    }








    //loot


    //progression


}
