﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Tools.Document
{
    public class LocalRecord
    {
        public LocalRecord(LocalizationDocument manager, String record)
        {
            _manager = manager;
            buffer = new string[manager.HeaderCount];
            if (String.IsNullOrWhiteSpace(record)) throw new Exception("记录不能为空白行");
            var arry = record.Trim().Split(new char[] { ',' });
            for (int i = 0; i < arry.Length && i < buffer.Length; i++)
            {
                buffer[i] = arry[i];
            }
        }


        public String Key
        {
            get
            {
                return getValue("Key");
            }
            set
            {
                setValue("Key", value);
            }
        }

        public String Source
        {
            get
            {
                return getValue("Source");
            }
            set
            {
                setValue("Source", value);
            }
        }

        public String Context
        {
            get
            {
                return getValue("Context");
            }
            set
            {
                setValue("Context", value);
            }
        }

        public String Changes
        {
            get
            {
                return getValue("Changes");
            }
            set
            {
                setValue("Changes", value);
            }
        }

        public String English
        {
            get
            {
                return getValue("English");
            }
            set
            {
                setValue("English", value);
            }
        }

        public String schinese
        {
            get
            {
                return getValue("schinese");
            }
            set
            {
                setValue("schinese", value);
            }
        }


        public override string ToString()
        {
            return String.Join(",", buffer);
        }

        private String getValue(String key)
        {
            var index = _manager.HeadersMap[key];
            return buffer[index];
        }

        private void setValue(String key, String value)
        {
            var index = _manager.HeadersMap[key];
            buffer[index] = value;
        }

        private LocalizationDocument _manager { get; set; }
        public String[] buffer { get; private set; }
    }
}
