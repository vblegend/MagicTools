﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows.Threading;

namespace Magic.Tools.Common
{
    public static class SystemResources
    {
        static SystemResources()
        {
            GameDirectory = @"E:\SteamLibrary\steamapps\common\7 Days To Die\";
            ItemIcons = new List<ItemIcon>();
        }
        public delegate void ResourceLoadCallBack(String name,Int32 total,Int32 current,Double bfb);



        /// <summary>
        /// 游戏目录.
        /// 计算机\HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Steam App 251570\InstallLocation
        /// </summary>
        public static String GameDirectory { get; set; }

        /// <summary>
        /// 游戏内物品图标
        /// </summary>
        public static List<ItemIcon> ItemIcons { get; private set; }


        public static void loadIcons(ResourceLoadCallBack callback)
        {
            Int32 index = 0;
            ItemIcons.Clear();
            var files = System.IO.Directory.GetFiles($@"{GameDirectory}\Data\ItemIcons", "*.png");
            ItemIcons = new List<ItemIcon>();
            callback?.Invoke("", files.Length, 0, 0);
            foreach (var file in files)
            {
                try
                {
                    index++;
                    var res = new ItemIcon()
                    {
                        //Image = new BitmapImage(new Uri(file, UriKind.Absolute)),
                        File = System.IO.Path.GetFileNameWithoutExtension(file),
                        FullFileName = file,
                    };
                    ItemIcons.Add(res);
                }
                catch (Exception)
                {
                }
                callback?.Invoke("", files.Length, index, (Double)index / files.Length * 100f);

            }
            callback?.Invoke("", files.Length, files.Length, 100);
        }
    }
}
