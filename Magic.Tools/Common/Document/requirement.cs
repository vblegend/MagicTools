﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class requirement
    {
        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }

        [XmlAttribute(AttributeName = "target")]
        public String target { get; set; }

        [XmlAttribute(AttributeName = "skill_name")]
        public String skill_name { get; set; }

        [XmlAttribute(AttributeName = "buff")]
        public String buff { get; set; }

        [XmlAttribute(AttributeName = "stat")]
        public String stat { get; set; }

        [XmlAttribute(AttributeName = "seed_type")]
        public String seed_type { get; set; }

        [XmlAttribute(AttributeName = "min_max")]
        public String min_max { get; set; }

        [XmlAttribute(AttributeName = "cvar")]
        public String cvar { get; set; }

        [XmlAttribute(AttributeName = "progression_name")]
        public String progression_name { get; set; }

        [XmlAttribute(AttributeName = "operation")]
        public String operation { get; set; }

        [XmlAttribute(AttributeName = "value")]
        public String value { get; set; }

        [XmlAttribute(AttributeName = "desc_key")]
        public String desc_key { get; set; }
        
        [XmlAttribute(AttributeName = "tags")]
        public String tags { get; set; }

        [XmlAttribute(AttributeName = "body_parts")]
        public String body_parts { get; set; }

        
    }
}
