﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using Magic.Tools.Common.Sound;

namespace Magic.Tools.Common
{
    public class SoundDataNode
    {
        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }

        [XmlElement(ElementName = "AudioSource")]
        public AudioSource AudioSource { get; set; }

        [XmlElement(ElementName = "NetworkAudioSource")]
        public NameProperty NetworkAudioSource { get; set; }


        [XmlElement(ElementName = "Noise")]
        public Noise Noise { get; set; }

        [XmlElement(ElementName = "AudioClip")]
        public List<AudioClip> AudioClips { get; set; }

        
        [XmlElement(ElementName = "IgnoreDistanceCheck")]
        public NameProperty IgnoreDistanceCheck { get; set; }

        [XmlElement(ElementName = "Channel")]
        public NameProperty Channel { get; set; }

        [XmlElement(ElementName = "Priority")]
        public NameProperty Priority { get; set; }

        [XmlElement(ElementName = "maxVoicesPerEntity")]
        public ValueProperty maxVoicesPerEntity { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [XmlElement(ElementName = "MaxVoicesPerEntity")]
        public ValueProperty MaxVoicesPerEntity { get; set; }




        [XmlElement(ElementName = "LowestPitch")]
        public NameProperty LowestPitch { get; set; }

        [XmlElement(ElementName = "HighestPitch")]
        public NameProperty HighestPitch { get; set; }


        [XmlElement(ElementName = "Sequence")]
        public NameProperty Sequence { get; set; }

        [XmlElement(ElementName = "Immediate")]
        public NameProperty Immediate { get; set; }


        //
        //


        //
        //





        [XmlElement(ElementName = "LocalCrouchVolumeScale")]
        public ValueProperty LocalCrouchVolumeScale { get; set; }

        [XmlElement(ElementName = "CrouchNoiseScale")]
        public ValueProperty CrouchNoiseScale { get; set; }

        [XmlElement(ElementName = "NoiseScale")]
        public ValueProperty NoiseScale { get; set; }

        [XmlElement(ElementName = "MaxVoices")]
        public ValueProperty MaxVoices { get; set; }

        [XmlElement(ElementName = "MaxEntities")]
        public NameProperty MaxEntities { get; set; }
        
        [XmlElement(ElementName = "MaxRepeatRate")]
        public ValueProperty MaxRepeatRate { get; set; }

        [XmlElement(ElementName = "runningvolumescale")]
        public ValueProperty runningvolumescale { get; set; }


        

    }
}
