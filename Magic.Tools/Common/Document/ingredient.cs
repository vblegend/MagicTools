﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class ingredient
    {
        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }

        [XmlAttribute(AttributeName = "count")]
        public String count { get; set; }
    }
}
