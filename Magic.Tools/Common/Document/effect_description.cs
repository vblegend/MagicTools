﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
   public class effect_description
    {

        [XmlAttribute(AttributeName = "level")]
        public String level { get; set; }

        [XmlAttribute(AttributeName = "desc_key")]
        public String desc_key { get; set; }

        [XmlAttribute(AttributeName = "long_desc_key")]
        public String long_desc_key { get; set; }



        [XmlAttribute(AttributeName = "desc_base")]
        public String desc_base { get; set; }

        

    }
}
