﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class requirements
    {

        [XmlAttribute(AttributeName = "compare_type")]
        public String compare_type { get; set; }

        [XmlElement(ElementName = "requirement")]
        public List<requirement> Requirements { get; set; }

    }
}
