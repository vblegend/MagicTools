﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class vehicle
    {
        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }

        [XmlElement(ElementName = "property")]
        public List<property> Propertys { get; set; }
    }
}
