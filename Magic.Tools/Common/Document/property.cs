﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class property
    {
        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }

        [XmlAttribute(AttributeName = "value")]
        public String value { get; set; }


        [XmlAttribute(AttributeName = "class")]
        public String Class { get; set; }


        [XmlAttribute(AttributeName = "param1")]
        public String param1 { get; set; }

        [XmlAttribute(AttributeName = "data")]
        public String data { get; set; }

        [XmlElement(ElementName = "property")]
        public List<property> Propertys { get; set; }


        [XmlElement(ElementName = "requirement")]
        public List<requirement> Requirements { get; set; }



        
    }
}
