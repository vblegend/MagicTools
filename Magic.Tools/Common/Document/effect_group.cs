﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class effect_group
    {
        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }

        [XmlElement(ElementName = "requirements")]
        public requirements requirements { get; set; }
        
        [XmlElement(ElementName = "requirement")]
        public List<requirement> Requirements { get; set; }

        [XmlElement(ElementName = "passive_effect")]
        public List<passive_effect> Passive_effects { get; set; }

        [XmlElement(ElementName = "triggered_effect")]
        public List<triggered_effect> Triggered_effects { get; set; }

        [XmlElement(ElementName = "display_value")]
        public List<display_value> Display_values { get; set; }

        [XmlAttribute(AttributeName = "tiered")]
        public String tiered { get; set; }



        [XmlElement(ElementName = "effect_description")]
        public List<effect_description> Effect_descriptions { get; set; }


        
    }
}
