﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class triggered_effect
    {
        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }

        [XmlAttribute(AttributeName = "trigger")]
        public String trigger { get; set; }


        [XmlAttribute(AttributeName = "event")]
        public String @event { get; set; }


        

        [XmlAttribute(AttributeName = "action")]
        public String action { get; set; }

        [XmlAttribute(AttributeName = "target")]
        public String target { get; set; }


        [XmlAttribute(AttributeName = "message")]
        public String message { get; set; }


        #region MyRegion
        [XmlAttribute(AttributeName = "effect_name")]
        public String effect_name { get; set; }

        [XmlAttribute(AttributeName = "intensity")]
        public String intensity { get; set; }

        [XmlAttribute(AttributeName = "fade")]
        public String fade { get; set; }


        #endregion






        [XmlAttribute(AttributeName = "duration")]
        public String duration { get; set; }

        [XmlAttribute(AttributeName = "force")]
        public String force { get; set; }






        [XmlAttribute(AttributeName = "sound")]
        public String sound { get; set; }

        [XmlAttribute(AttributeName = "play_in_head")]
        public String play_in_head { get; set; }

        #region AddPart RemovePart
        [XmlAttribute(AttributeName = "part")]
        public String part { get; set; }

        [XmlAttribute(AttributeName = "prefab")]
        public String prefab { get; set; }

        [XmlAttribute(AttributeName = "parentTransform")]
        public String parentTransform { get; set; }

        [XmlAttribute(AttributeName = "localPos")]
        public String localPos { get; set; }

        [XmlAttribute(AttributeName = "localRot")]
        public String localRot { get; set; }

        #endregion

        [XmlAttribute(AttributeName = "active")]
        public String active { get; set; }




        #region AddBuff 2
        [XmlAttribute(AttributeName = "range")]
        public String range { get; set; }

        [XmlAttribute(AttributeName = "target_tags")]
        public String target_tags { get; set; }

        #endregion

        [XmlAttribute(AttributeName = "reset_books")]
        public String reset_books { get; set; }

        [XmlAttribute(AttributeName = "message_key")]
        public String message_key { get; set; }






        [XmlAttribute(AttributeName = "progression_name")]
        public String progression_name { get; set; }


        [XmlAttribute(AttributeName = "level")]
        public String level { get; set; }


        [XmlAttribute(AttributeName = "journal")]
        public String journal { get; set; }


        [XmlAttribute(AttributeName = "fireOneBuff")]
        public String fireOneBuff { get; set; }


        [XmlAttribute(AttributeName = "stat")]
        public String stat { get; set; }



        [XmlAttribute(AttributeName = "cvar")]
        public String cvar { get; set; }

        [XmlAttribute(AttributeName = "operation")]
        public String operation { get; set; }

        [XmlAttribute(AttributeName = "value")]
        public String value { get; set; }


        [XmlAttribute(AttributeName = "buff")]
        public String buff { get; set; }

        [XmlAttribute(AttributeName = "exp")]
        public String exp { get; set; }

        [XmlAttribute(AttributeName = "weights")]
        public String weights { get; set; }



        [XmlAttribute(AttributeName = "particle")]
        public String particle { get; set; }

        [XmlAttribute(AttributeName = "local_offset")]
        public String local_offset { get; set; }

        [XmlAttribute(AttributeName = "parent_transform")]
        public String parent_transform { get; set; }

        [XmlAttribute(AttributeName = "transform_path")]
        public String transform_path { get; set; }


        [XmlAttribute(AttributeName = "local_rotation")]
        public String local_rotation { get; set; }

        







        [XmlElement(ElementName = "requirement")]
        public List<requirement> Requirements { get; set; }
    }
}
