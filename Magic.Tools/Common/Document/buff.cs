﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
   public class buff
    {
        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }



        [XmlAttribute(AttributeName = "description_key")]
        public String description_key { get; set; }

        [XmlAttribute(AttributeName = "tooltip_key")]
        public String tooltip_key { get; set; }

        [XmlAttribute(AttributeName = "icon")]
        public String icon { get; set; }


        [XmlAttribute(AttributeName = "name_key")]
        public String name_key { get; set; }

        [XmlAttribute(AttributeName = "icon_color")]
        public String icon_color { get; set; }

        [XmlAttribute(AttributeName = "hidden")]
        public String hidden { get; set; }

        [XmlAttribute(AttributeName = "icon_blink")]
        public String icon_blink { get; set; }

        [XmlAttribute(AttributeName = "remove_on_death")]
        public String remove_on_death { get; set; }







        [XmlElement(ElementName = "damage_type")]
        public ValueProperty damage_type { get; set; }


        [XmlElement(ElementName = "damage_source")]
        public ValueProperty damage_source { get; set; }

        [XmlElement(ElementName = "stack_type")]
        public ValueProperty stack_type { get; set; }


        [XmlElement(ElementName = "update_rate")]
        public ValueProperty update_rate { get; set; }

        [XmlElement(ElementName = "duration")]
        public ValueProperty duration { get; set; }

        [XmlElement(ElementName = "display_value_key")]
        public ValueProperty display_value_key { get; set; }


        [XmlElement(ElementName = "display_value")]
        public ValueProperty display_value { get; set; }

        [XmlElement(ElementName = "display_value_format")]
        public ValueProperty display_value_format { get; set; }


        


        [XmlElement(ElementName = "effect_group")]
        public List<effect_group> Effect_groups { get; set; }



        //[XmlElement(ElementName = "triggered_effect")]
        //public List<triggered_effect> Triggered_effects { get; set; }

        //[XmlElement(ElementName = "passive_effect")]
        //public List<passive_effect> Passive_effects { get; set; }

    }
}
