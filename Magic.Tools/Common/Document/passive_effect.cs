﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class passive_effect
    {
        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }

        [XmlAttribute(AttributeName = "level")]
        public String level { get; set; }

        [XmlAttribute(AttributeName = "operation")]
        public String operation { get; set; }

        [XmlAttribute(AttributeName = "value")]
        public String value { get; set; }

        [XmlAttribute(AttributeName = "duration")]
        public String duration { get; set; }

        


        [XmlAttribute(AttributeName = "tier")]
        public String tier { get; set; }

        [XmlAttribute(AttributeName = "tags")]
        public String tags { get; set; }

        [XmlAttribute(AttributeName = "match_all_tags")]
        public String match_all_tags { get; set; }

        [XmlElement(ElementName = "requirement")]
        public List<requirement> Requirements { get; set; }
    }
}
