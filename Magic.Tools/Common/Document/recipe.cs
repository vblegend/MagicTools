﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class recipe
    {
        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }


        [XmlAttribute(AttributeName = "count")]
        public String count { get; set; }

        [XmlAttribute(AttributeName = "tooltip")]
        public String tooltip { get; set; }


        [XmlAttribute(AttributeName = "material_based")]
        public String material_based { get; set; }


        [XmlAttribute(AttributeName = "craft_time")]
        public String craft_time { get; set; }

        [XmlAttribute(AttributeName = "craft_area")]
        public String craft_area { get; set; }


        [XmlAttribute(AttributeName = "craft_tool")]
        public String craft_tool { get; set; }


        [XmlAttribute(AttributeName = "craft_exp_gain")]
        public String craft_exp_gain { get; set; }

        [XmlAttribute(AttributeName = "always_unlocked")]
        public String always_unlocked { get; set; }

        [XmlAttribute(AttributeName = "use_ingredient_modifier")]
        public String use_ingredient_modifier { get; set; }


        [XmlAttribute(AttributeName = "tags")]
        public String tags { get; set; }

        [XmlElement(ElementName = "wildcard_forge_category")]
        public wildcard_forge_category wildcard_forge_category { get; set; }

        [XmlElement(ElementName = "ingredient")]
        public List<ingredient> Ingredients { get; set; }

        [XmlElement(ElementName = "effect_group")]
        public List<effect_group> Effect_groups { get; set; }


    }
}
