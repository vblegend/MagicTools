﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class item_modifier
    {
        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }

        [XmlAttribute(AttributeName = "installable_tags")]
        public String installable_tags { get; set; }


        [XmlAttribute(AttributeName = "modifier_tags")]
        public String modifier_tags { get; set; }

        [XmlAttribute(AttributeName = "blocked_tags")]
        public String blocked_tags { get; set; }

        [XmlAttribute(AttributeName = "type")]
        public String type { get; set; }

        [XmlAttribute(AttributeName = "rarity")]
        public String rarity { get; set; }

        [XmlAttribute(AttributeName = "cosmetic_install_chance")]
        public String cosmetic_install_chance { get; set; }

        

        [XmlElement(ElementName = "property")]
        public List<property> Propertys { get; set; }

        [XmlElement(ElementName = "item_property_overrides")]
        public List<item_property_overrides> Item_property_overrides { get; set; }

        [XmlElement(ElementName = "effect_group")]
        public List<effect_group> Effect_groups { get; set; }
    }
}
