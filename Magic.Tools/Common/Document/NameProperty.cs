﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class NameProperty
    {

        [XmlAttribute(AttributeName = "name")]
        public String Name { get; set; }
    }

}
