﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class block
    {

        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }

        [XmlElement(ElementName = "property")]
        public List<property> Propertys { get; set; }

        [XmlElement(ElementName = "drop")]
        public List<drop> Drops { get; set; }
    }
}
