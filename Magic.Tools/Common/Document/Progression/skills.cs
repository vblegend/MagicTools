﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common.Progression
{
    public class skills
    {

        [XmlAttribute(AttributeName = "min_level")]
        public String min_level { get; set; }

        [XmlAttribute(AttributeName = "max_level")]
        public String max_level { get; set; }

        [XmlElement(ElementName = "skill")]
        public List<skill> Skills { get; set; }
    }
}
