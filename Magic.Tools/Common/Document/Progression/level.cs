﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common.Progression
{
    public class level
    {
        [XmlAttribute(AttributeName = "max_level")]
        public String max_level { get; set; }

        [XmlAttribute(AttributeName = "exp_to_level")]
        public String exp_to_level { get; set; }

        [XmlAttribute(AttributeName = "experience_multiplier")]
        public String experience_multiplier { get; set; }

        [XmlAttribute(AttributeName = "skill_points_per_level")]
        public String skill_points_per_level { get; set; }

        [XmlAttribute(AttributeName = "clamp_exp_cost_at_level")]
        public String clamp_exp_cost_at_level { get; set; }
    }
}
