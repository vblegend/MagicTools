﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common.Progression
{
    public class perks
    {

        [XmlAttribute(AttributeName = "min_level")]
        public String min_level { get; set; }

        [XmlAttribute(AttributeName = "max_level")]
        public String max_level { get; set; }

        [XmlAttribute(AttributeName = "base_skill_point_cost")]
        public String base_skill_point_cost { get; set; }

        [XmlAttribute(AttributeName = "cost_multiplier_per_level")]
        public String cost_multiplier_per_level { get; set; }

        [XmlAttribute(AttributeName = "max_level_ratio_to_parent")]
        public String max_level_ratio_to_parent { get; set; }


        


        [XmlElement(ElementName = "perk")]
        public List<perk> Perks { get; set; }
    }
}
