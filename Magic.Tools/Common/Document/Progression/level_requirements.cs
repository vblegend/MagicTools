﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common.Progression
{
    public class level_requirements
    {
        [XmlAttribute(AttributeName = "level")]
        public String level { get; set; }

        [XmlElement(ElementName = "requirement")]
        public List<requirement> Requirements { get; set; }

    }
}
