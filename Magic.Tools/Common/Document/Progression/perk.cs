﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common.Progression
{
   public class perk
    {

        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }

        [XmlAttribute(AttributeName = "max_level")]
        public String max_level { get; set; }

        [XmlAttribute(AttributeName = "parent")]
        public String parent { get; set; }

        [XmlAttribute(AttributeName = "name_key")]
        public String name_key { get; set; }


        [XmlAttribute(AttributeName = "desc_key")]
        public String desc_key { get; set; }


        [XmlAttribute(AttributeName = "icon")]
        public String icon { get; set; }




        [XmlAttribute(AttributeName = "base_skill_point_cost")]
        public String base_skill_point_cost { get; set; }


        [XmlAttribute(AttributeName = "long_desc_key")]
        public String long_desc_key { get; set; }







        [XmlElement(ElementName = "level_requirements")]
        public List<level_requirements> level_requirements { get; set; }


        [XmlElement(ElementName = "effect_group")]
        public List<effect_group> Effect_groups { get; set; }
    }
}
