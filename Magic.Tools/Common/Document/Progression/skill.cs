﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common.Progression
{
  public  class skill
    {


        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }


        [XmlAttribute(AttributeName = "parent")]
        public String parent { get; set; }


        [XmlAttribute(AttributeName = "name_key")]
        public String name_key { get; set; }


        [XmlAttribute(AttributeName = "desc_key")]
        public String desc_key { get; set; }

        [XmlAttribute(AttributeName = "long_desc_key")]
        public String long_desc_key { get; set; }



        

        [XmlAttribute(AttributeName = "icon")]
        public String icon { get; set; }



        [XmlElement(ElementName = "effect_group")]
        public List<effect_group> Effect_groups { get; set; }


    }
}
