﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class drop
    {
        [XmlAttribute(AttributeName = "event")]
        public String @event { get; set; }

        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }

        [XmlAttribute(AttributeName = "count")]
        public String count { get; set; }

        [XmlAttribute(AttributeName = "tag")]
        public String tag { get; set; }

        [XmlAttribute(AttributeName = "prob")]
        public String prob { get; set; }

        [XmlAttribute(AttributeName = "tool_category")]
        public String tool_category { get; set; }

        [XmlAttribute(AttributeName = "stick_chance")]
        public String stick_chance { get; set; }

    }
}
