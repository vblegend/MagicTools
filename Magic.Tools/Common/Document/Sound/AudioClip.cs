﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common.Sound
{
    public class AudioClip
    {

        [XmlAttribute(AttributeName = "ClipName")]
        public String ClipName { get; set; }

        [XmlAttribute(AttributeName = "Loop")]
        public Boolean Loop { get; set; }

        [XmlAttribute(AttributeName = "AudioSourceName")]
        public String AudioSourceName { get; set; }


        


    }
}
