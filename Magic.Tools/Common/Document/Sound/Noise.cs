﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common.Sound
{
    public class Noise
    {

        [XmlAttribute(AttributeName = "ID")]
        public String ID { get; set; }

        [XmlAttribute(AttributeName = "noise")]
        public String noise { get; set; }

        [XmlAttribute(AttributeName = "time")]
        public String time { get; set; }

        [XmlAttribute(AttributeName = "muffled_when_crouched")]
        public String muffled_when_crouched { get; set; }

        [XmlAttribute(AttributeName = "heat_map_strength")]
        public String heat_map_strength { get; set; }

        [XmlAttribute(AttributeName = "heat_map_time")]
        public String heat_map_time { get; set; }

    }
}
