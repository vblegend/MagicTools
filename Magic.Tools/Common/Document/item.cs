﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Magic.Tools.Common
{
    public class item
    {

        [XmlAttribute(AttributeName = "name")]
        public String name { get; set; }

        [XmlElement(ElementName = "property")]
        public List<property> Propertys { get; set; }

        [XmlElement(ElementName = "Property")]
        public List<property> Propertys2 { get; set; }

        [XmlElement(ElementName = "effect_group")]
        public List<effect_group> Effect_groups { get; set; }

    }

}
