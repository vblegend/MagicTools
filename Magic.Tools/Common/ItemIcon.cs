﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Magic.Tools.Common
{
    public class ItemIcon
    {
        public ImageSource Image { get; set; }
        public String File { get; set; }
        public String Description { get; set; }
        public String FullFileName { get; set; }
    }

}
