﻿using PropertyTools.DataAnnotations;
using PropertyTools.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Magic.Tools.Models.Propertys
{
    public class TestPropertyProxy 
    {
        public TestPropertyProxy()
        {
          //  ToolTip = "aaaaaaa";
        }
        #region Private Property

        //[Browsable(false)]
        //public String ToolTip { get; set; }




        [Category("公共属性|基本属性")]
        //[EnableBy("Placeholder", 0)]
        [DisplayName("对象ID")]
        //[HorizontalAlignment(HorizontalAlignment.Right)]
        public Guid Id
        {
            get;set;
        }


        [Category("公共属性|基本属性")]
        [AutoUpdateText]
        [DisplayName("旋转角度")]
        [Spinnable(1, 10, -360, 360)]
        public Double Angle
        {
            get; set;
        }


        [Category("公共属性|基本属性")]
        [DisplayName("文本内容")]
        [AutoUpdateText]
        public String Content
        {
            get; set;
        }
        #region Text


        [Category("公共属性|样式属性")]
        [DisplayName("文本字体")]
        [FontPreview(12)]
        public FontFamily Font
        {
            get; set;

        }

        [Category("公共属性|样式属性")]
        [DisplayName("字体大小")]
        [Spinnable(0.1, 1, 8, 100)]
        [AutoUpdateText]
        public Double FontSize
        {
            get; set;

        }

        [Category("公共属性|样式属性")]
        [DisplayName("粗体")]
        public Boolean FontBold
        {
            get; set;
        }



        [Category("公共属性2|样式属性")]
        [DisplayName("字体颜色")]
        [Converter(typeof(ColorToBrushConverter))]
        public SolidColorBrush Color
        {
            get; set;
        }



        [Category("公共属性|样式属性")]
        [DisplayName("轮廓颜色")]
        [Converter(typeof(ColorToBrushConverter))]
        public SolidColorBrush EdgeColor
        {
            get; set;
        }




        [Category("公共属性|样式属性")]
        [DisplayName("轮廓宽度")]
        //[DependsOn(nameof(Shape))]
        [Editable(false)]
        [FormatString("0.0")]
        [Spinnable(0.1, 1, 0.1, 15)]
        [AutoUpdateText]
        public Double EdgeThickness
        {
            get; set;
        }








        [Category("公共属性|样式属性")]
        [DisplayName("对齐方式")]
        [SelectorStyle(SelectorStyle.ComboBox)]
        public TextAlignment Alignment
        {
            get; set;
        }

        #endregion

        #region Position & Size 
        [Category("公共属性|位置大小")]
        [Spinnable(0.01, 1, 0, 1920)]
        [DisplayName("左侧")]
        [AutoUpdateText]
        public Double X
        {
            get; set;
        }

        [Category("公共属性|位置大小")]
        [Spinnable(0.01, 1, 0, 1080)]
        [DisplayName("上侧")]
        [AutoUpdateText]
        public Double Y
        {
            get; set;
        }

        [Category("公共属性|位置大小")]
        [Spinnable(0.01, 1, 0, 1920)]
        [DisplayName("宽度")]
        [AutoUpdateText]
        public Double Width
        {
            get; set;
        }

        [Category("公共属性|位置大小")]
        [Spinnable(0.01, 1, 0, 1080)]
        [DisplayName("高度")]
        [AutoUpdateText]
        public Double Height
        {
            get
            {
                return 1234;
            }
        }
        #endregion



        [Category("公共属性|鼠标事件")]
        [DisplayName("响应事件")]
        public Boolean HandleEvent
        {
            get; set;
        }





        


        #endregion
    }
}
