﻿using Magic.Models.Controls;
using System;

namespace Magic.Tools.Models.Tables
{
    public class UIDesignerTableItem : CostomTabItem
    {
        public UIDesignerTableItem()
        {
            this.IsChanged = true;
        }

        protected override void OnClosed(object param)
        {
            Console.WriteLine(this.Title);
            base.OnClosed(param);
        }


        protected override void OnSelected()
        {
            
        }


        #region Private Property

        #endregion

    }

}
