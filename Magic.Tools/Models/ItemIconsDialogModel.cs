﻿using Magic.Tools.Common;
using Magic.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Tools.Models
{
    public class ItemIconsDialogModel : DialogModel
    {
        public ItemIconsDialogModel()
        {
            this.Title = "选择物品图标";
            SearchAction = this.Search;
            ItemIcons = new ObservableCollection<ItemIcon>(SystemResources.ItemIcons);
        }


        public ObservableCollection<ItemIcon> ItemIcons
        {
            get
            {
                return base.GetValue<ObservableCollection<ItemIcon>>();
            }
            set
            {
                base.SetValue(value);
            }
        }





        public void Search(Object text)
        {
            var key = text as String;
            if (ItemIcons != null)
            {
                ItemIcons.Clear();
                ItemIcons = null;
            }
            if (String.IsNullOrEmpty(key))
            {
                ItemIcons = new ObservableCollection<ItemIcon>(SystemResources.ItemIcons);
            }
            else
            {
                var ls = from s in SystemResources.ItemIcons where s.File.ToLower().Contains(key.ToLower()) || (!String.IsNullOrEmpty(s.Description) && s.Description.ToLower().Contains(key.ToLower())) select s;
                ItemIcons = new ObservableCollection<ItemIcon>(ls);
            }
        }







        public Action<Object> SearchAction { get; private set; }


        public override void Dispose()
        {
            this.ItemIcons.Clear();
            base.Dispose();
        }
    }
}
