﻿using Magic.Tools.Common;
using Magic.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Magic.Tools.Models
{
    public class TestModel : DialogModel
    {
        public TestModel()
        {
            this.Title = "测试窗口";
            HelpCommand = base.CreatedCommand(Help_Click);
            ItemsSource = new ObservableCollection<Guid>();
        }

        public void Help_Click(Object param)
        {
            ItemsSource.Add(Guid.NewGuid());
            //MessageBox.Show("Help");
        }





       public ObservableCollection<Guid> ItemsSource
        {
            get
            {
                return base.GetValue<ObservableCollection<Guid>>();
            }
            set
            {
                base.SetValue(value);
            }
       }









        /// <summary>
        /// 应用,需要时在派生类中重写
        /// </summary>
        public ICommand HelpCommand { get; protected set; }



        public override void Dispose()
        {

            base.Dispose();
        }
    }
}
