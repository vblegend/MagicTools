﻿using Magic.Tools.Models.Tables;
using Magic.Tools.Views;
using Magic.Actions;
using Magic.Models.Controls;
using Magic.Uitity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using Magic.Tools.Models.Propertys;
using Magic.Core;
using Magic.Controls;
using Magic.Tools.Controls;
using System.Windows.Media;
using System.Windows.Media.Effects;

namespace Magic.Tools.Models
{
    public class MainWindowModel : DialogModel
    {



        public TestPropertyProxy Proxy
        {
            get
            {
                return base.GetValue<TestPropertyProxy>();
            }
            set
            {
                base.SetValue(value);
            }
        }




        public MainWindowModel()
        {
            this.Title = "Magic Tools";
            TableItems = new ObservableCollection<CostomTabItem>();


            TableItems.Add(new UIDesignerTableItem() { Content = new UIDesignerPage(), Title = "designer.json", ToolTip = "this is designer window" });
            TableItems.Add(new CodeEditorTableItem() { Content = new CodeEditorPage(), Title = "Code.json", ToolTip = "this is Code.Json" });





            TableItems.Add(new ProjectTableItem() { Content = new TestPage(), Title = "Test.json", ToolTip = "this is Project.Json" });
            Proxy = new TestPropertyProxy();

            ProjectViewRoot = new ObservableCollection<CustomTreeViewItem>();
            SelectedItemIconCommand = base.CreatedCommand(selecticon);
            ThemesCommand = base.CreatedCommand(Themes_Click);
            HelpCommand = base.CreatedCommand(Help_Click);
            //
            Search_TextChanged_EventAction = new EventActionDelegate(Search_TextChanged);
            buildProject();
            this.BorderThickness = new Thickness(0);
        }



        private void selecticon(Object param)
        {
            //
            //var dialog = new Test(); ItemIconsDialog
            //dialog.Show();
            var dialog2 = new Test2();
            dialog2.ShowDialog();

        }


        private void Themes_Click(Object param)
        {
            if (ThemeManager.CurrentTheme == "Default")
            {
                ThemeManager.LoadTheme("Themes\\White.xaml");
            }
            else if (ThemeManager.CurrentTheme == "White")
            {
                ThemeManager.LoadTheme("Themes\\Black.xaml");
            }
            else if (ThemeManager.CurrentTheme == "Black")
            {
                ThemeManager.LoadTheme("Themes\\Image.xaml");
            }
            else
            {
                ThemeManager.LoadTheme("Themes\\White.xaml");
            }
            //Console.WriteLine(ThemeManager.CurrentTheme);
        }

        private void Help_Click(Object param)
        {
            if (this.BorderThickness.Bottom == 1)
            {
                this.BorderThickness = new Thickness(0);
            }
            else
            {
                this.BorderThickness = new Thickness(1);
            }
      
        }



        private void buildProject()
        {
            var project = new CustomTreeViewItem() { Header = $"新建项目" };
            project.loadIcon("/Magic.Tools;component/Images/Icon.png");
            ProjectViewRoot.Add(project);
            //<Image "/Magic.Tools;component/Images/min_button.png" />
            for (int i = 0; i < 5; i++)
            {
                var it = new CustomTreeViewItem() { Header = $"Root_{i}" };
                it.ExpandedChanged += It_ExpandedChanged;
                it.loadIcon("/Magic.Tools;component/Images/Content/folder.closed.png");
                project.Children.Add(it);
                for (int x = 0; x < 6; x++)
                {
                    var ip = new CustomTreeViewItem() { Header = $"Child_{x}" };

                    ip.loadIcon("/Magic.Tools;component/Images/Content/file.png");
                    //ip.loadPath();
                    it.Children.Add(ip);
                }
            }
        }

        private void It_ExpandedChanged(CustomTreeViewItem sender, bool isExpanded)
        {
            if (isExpanded)
            {
                sender.loadIcon("/Magic.Tools;component/Images/Content/folder.opend.png");
            }
            else
            {
                sender.loadIcon("/Magic.Tools;component/Images/Content/folder.closed.png");
            }

        }

        public ObservableCollection<CustomTreeViewItem> ProjectViewRoot
        {
            get
            {
                return base.GetValue<ObservableCollection<CustomTreeViewItem>>();
            }
            set
            {
                base.SetValue(value);
            }
        }





        public ObservableCollection<CostomTabItem> TableItems
        {
            get
            {
                return base.GetValue<ObservableCollection<CostomTabItem>>();
            }
            set
            {
                base.SetValue(value);
            }
        }

        public void Search_TextChanged(FrameworkElement sender, RoutedEventArgs args)
        {
            Console.WriteLine(sender);

        }

        public EventActionDelegate Search_TextChanged_EventAction { get; set; }

        public Thickness BorderThickness
        {
            get
            {
                return base.GetValue<Thickness>();
            }
            set
            {
                base.SetValue(value);
            }
        }






        /// <summary>
        /// 应用,需要时在派生类中重写
        /// </summary>
        public ICommand SelectedItemIconCommand { get; protected set; }


        /// <summary>
        /// 应用,需要时在派生类中重写
        /// </summary>
        public ICommand HelpCommand { get; protected set; }


        /// <summary>
        /// 应用,需要时在派生类中重写
        /// </summary>
        public ICommand ThemesCommand { get; protected set; }


    }
}
