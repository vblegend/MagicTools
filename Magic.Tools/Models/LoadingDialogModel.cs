﻿using Magic.Tools.Common;
using Magic.Core;
using Magic.Uitity;
using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace Magic.Tools.Models
{
    public class LoadingDialogModel : DialogModel
    {
        public LoadingDialogModel()
        {
            progressValue = 0;
            progressText = "";
            currentprogressText = "";
            //ThemeManager.LoadTheme("Themes\\Black.xaml");
        }

        public override void InitializeModel()
        {
            init();
            //SystemResources.loadIcons((n, t, c, b) =>
            //{
            //        progressText = $"{c}/{t}";
            //        progressValue = b;
            //});
            //this.DialogResult = true;
        }



        private async Task init()
        {
            await Task.Run(() =>
            {
                //AsyncTask.SwitchToUI(() =>
                //{
                //    SystemResources.loadIcons((n, t, c, b) =>
                //    {

                //            progressText = $"{c}/{t}";
                //            progressValue = b;
      
                //    });
                //});



                AsyncTask.SwitchToUI(() =>
                {
                    this.DialogResult = true;
                });
            });
        }




        public Double progressValue
        {
            get
            {
                return base.GetValue<Double>();
            }
            set
            {
                base.SetValue(value);
            }
        }





        public String progressText
        {
            get
            {
                return base.GetValue<String>();
            }
            set
            {
                base.SetValue(value);
            }
        }


        public String currentprogressText
        {
            get
            {
                return base.GetValue<String>();
            }
            set
            {
                base.SetValue(value);
            }
        }
        

    }
}
